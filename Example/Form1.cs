﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Example.Devices;

namespace Example
{
    public partial class Form1 : Form
    {
        private DeviceRegister.SmCredentials _cred;
        private Dictionary<string, DeviceRegister.DeviceInfo> _devices = new Dictionary<string, DeviceRegister.DeviceInfo>();
        private Camera _selectedCamera;
        private RTPReciever _rtpReciever;
        private string _myIp;

        public Form1()
        {
            InitializeComponent();
            System.Net.ServicePointManager.Expect100Continue = false;
            _rtpReciever = new RTPReciever();

            checkBoxIs192.Checked = true;
            checkBoxIs192_CheckedChanged(this, EventArgs.Empty);
        }

        private void GetDevices_Click(object sender, EventArgs e)
        {
            _cred = new DeviceRegister.SmCredentials() { Ip = textBoxIp.Text, Port = ushort.Parse(textBoxPort.Text), 
                Username = textBoxUsername.Text, Password = textBoxPasswd.Text };
            DeviceRegister.Register register = new DeviceRegister.Register(_cred);

            if (register.Open())
            {
                _devices = register.GetDevices(new DeviceRegister.DeviceType[] { DeviceRegister.DeviceType.Camera });
                register.Close();
                PopulateDevices();
            }
            else
            {
                _cred = null;
                _devices.Clear();
            }
        }

        private void PopulateDevices()
        {
            listBoxCameras.Items.Clear();
            var cameraDevices = _devices.Values.Where(info => info.Attributes["SYS_UpnpDeviceType"] == "urn:schemas-pelco-com:device:Camera:1");
            foreach (var device in cameraDevices)
                listBoxCameras.Items.Add(new Camera(device));
        }

        private void Cameras_MouseDown(object sender, MouseEventArgs e)
        {
            if (listBoxCameras.SelectedItem == null) return;
            _selectedCamera = listBoxCameras.SelectedItem as Camera;
        }

        private void buttonStream_Click(object sender, EventArgs e)
        {
            var param = _selectedCamera.Query();
            param.streamSession.transportURL = string.Format("rtp://{0}:{1}", _myIp, RTPReciever.ListenPort);
            _selectedCamera.Connect(param);
            _selectedCamera.Play(1);
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            _selectedCamera.Disconnect();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(_selectedCamera != null)
                _selectedCamera.Disconnect();
            if(_rtpReciever != null)
                _rtpReciever.Dispose();
        }

        private void checkBoxIs192_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxIs192.Checked)
            {
                textBoxIp.Text = "192.168.5.10";
                _myIp = Utils.MyIP("192.168.5");
            }
            else
            {
                textBoxIp.Text = "10.221.224.103";
                _myIp = Utils.MyIP("10.");
            }
        }
    }
}
