﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example.Devices
{
    class Camera
    {
        private DeviceRegister.DeviceInfo _camera;
        private VideoOutputNs.VideoOutput _videoOutput;
        private StreamControlNs.StreamControl _streamControl;
        private string _sessionId;

        public Camera(DeviceRegister.DeviceInfo camera)
        {
            _camera = camera;
            string ddfUri = _camera.Attributes["SYS_UpnpDevDescUrl"];
            _videoOutput = new VideoOutputNs.VideoOutput();
            string serviceId = _camera.Services.FirstOrDefault(info => info.Type == "urn:schemas-pelco-com:service:VideoOutput:1").Id;
            _videoOutput.Url = Utils.ControlUrl(ddfUri, serviceId);
            _streamControl = new StreamControlNs.StreamControl();
            serviceId = _camera.Services.FirstOrDefault(info => info.Type == "urn:schemas-pelco-com:service:StreamControl:1").Id;
            _streamControl.Url = Utils.ControlUrl(ddfUri, serviceId);
        }

        public string FriendlyName
        {
            get
            {
                string name = string.Empty;
                if(_camera.Attributes.ContainsKey("friendlyName"))
                    name = _camera.Attributes["friendlyName"];
                else if(_camera.Attributes.ContainsKey("SYS_UpnpFriendlyName"))
                    name = _camera.Attributes["SYS_UpnpFriendlyName"];
                return name;
            }
        }

        public bool IsOnline
        {
            get
            {
                return _camera.State == DeviceRegister.UpnpState.Online;
            }
        }

        public string Uuid
        {
            get
            {
                return _camera.UDN;
            }
        }

        public List<DeviceRegister.ServiceInfo> Services
        {
            get
            {
                return _camera.Services;
            }
        }

        public VideoOutputNs.StreamParameters Query()
        { 
            int asyncId = 0;
            bool asyncIdSpecified = false;
            VideoOutputNs.StreamCatalog catalog = _videoOutput.Query(new VideoOutputNs.StreamQuery(), ref asyncId, ref asyncIdSpecified);
            return catalog.entries.First();
        }

        public void Connect(VideoOutputNs.StreamParameters parameters)
        {
            int asyncId = 0;
            bool asyncIdSpecified = false;
            _videoOutput.Connect(ref parameters, ref asyncId, ref asyncIdSpecified);

            if (!string.IsNullOrWhiteSpace(parameters.streamSession.sessionId))
                _sessionId = parameters.streamSession.sessionId;
        }

        public void Play(float speed)
        {
            if (!string.IsNullOrWhiteSpace(_sessionId))
            { 
                int asyncId = 0;
                int streamPos;
                bool streamPosSpecified;
                _streamControl.Play(_sessionId, speed, 249990, "1", ref asyncId, out streamPos, out streamPosSpecified);
            }
        }

        public void Disconnect()
        {
            if (!string.IsNullOrWhiteSpace(_sessionId))
            {
                int asyncId = 0;
                bool asyncIdSpecified = false;
                _videoOutput.Disconnect(_sessionId, ref asyncId, ref asyncIdSpecified);
                _sessionId = string.Empty;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}{1}", FriendlyName, (IsOnline ? string.Empty : " - (Disabled)"));
        }
    }
}
