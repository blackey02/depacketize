﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;

namespace Example.Devices
{
    static class Utils
    {
        public static string ControlUrl(string ddfUri, string serviceId)
        {
            var match = Regex.Match(ddfUri, @"(?<=//)[\d.]+");
            string ip = match.Value;
            match = Regex.Match(ddfUri, @"(?<=:)[\d]+");
            string port = match.Value;
            match = Regex.Match(serviceId, @"(?<=:)[\w-]+$");
            string serviceIdEnding = match.Value;
            string controlUrl = string.Format("http://{0}:{1}/control/{2}", ip, port, serviceIdEnding);
            Console.WriteLine(ip);
            return controlUrl;
        }

        public static string MyIP(string startsWith)
        {
            IPHostEntry host;
            string localIP = string.Empty;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    var possibleIp = ip.ToString();
                    if (possibleIp.StartsWith(startsWith))
                    {
                        localIP = possibleIp;
                        break;
                    }
                }
            }
            return localIP;
        }
    }
}
