﻿namespace Example
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxCameras = new System.Windows.Forms.ListBox();
            this.buttonGetDevices = new System.Windows.Forms.Button();
            this.textBoxIp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPasswd = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.buttonStream = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.checkBoxIs192 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // listBoxCameras
            // 
            this.listBoxCameras.FormattingEnabled = true;
            this.listBoxCameras.Location = new System.Drawing.Point(12, 21);
            this.listBoxCameras.Name = "listBoxCameras";
            this.listBoxCameras.Size = new System.Drawing.Size(350, 316);
            this.listBoxCameras.TabIndex = 1;
            this.listBoxCameras.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Cameras_MouseDown);
            // 
            // buttonGetDevices
            // 
            this.buttonGetDevices.Location = new System.Drawing.Point(12, 343);
            this.buttonGetDevices.Name = "buttonGetDevices";
            this.buttonGetDevices.Size = new System.Drawing.Size(95, 23);
            this.buttonGetDevices.TabIndex = 7;
            this.buttonGetDevices.Text = "Get Devices";
            this.buttonGetDevices.UseVisualStyleBackColor = true;
            this.buttonGetDevices.Click += new System.EventHandler(this.GetDevices_Click);
            // 
            // textBoxIp
            // 
            this.textBoxIp.Location = new System.Drawing.Point(12, 388);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new System.Drawing.Size(83, 20);
            this.textBoxIp.TabIndex = 9;
            this.textBoxIp.Text = "10.221.224.103";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 372);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "SM IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 372);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "SM Port";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(101, 388);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(52, 20);
            this.textBoxPort.TabIndex = 12;
            this.textBoxPort.Text = "60001";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 413);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "SM Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 413);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "SM Passwd";
            // 
            // textBoxPasswd
            // 
            this.textBoxPasswd.Location = new System.Drawing.Point(101, 429);
            this.textBoxPasswd.Name = "textBoxPasswd";
            this.textBoxPasswd.Size = new System.Drawing.Size(63, 20);
            this.textBoxPasswd.TabIndex = 16;
            this.textBoxPasswd.Text = "admin";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(14, 429);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(63, 20);
            this.textBoxUsername.TabIndex = 17;
            this.textBoxUsername.Text = "admin";
            // 
            // buttonStream
            // 
            this.buttonStream.Location = new System.Drawing.Point(265, 343);
            this.buttonStream.Name = "buttonStream";
            this.buttonStream.Size = new System.Drawing.Size(95, 23);
            this.buttonStream.TabIndex = 18;
            this.buttonStream.Text = "Stream";
            this.buttonStream.UseVisualStyleBackColor = true;
            this.buttonStream.Click += new System.EventHandler(this.buttonStream_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(265, 372);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(95, 23);
            this.buttonStop.TabIndex = 19;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // checkBoxIs192
            // 
            this.checkBoxIs192.AutoSize = true;
            this.checkBoxIs192.Location = new System.Drawing.Point(265, 413);
            this.checkBoxIs192.Name = "checkBoxIs192";
            this.checkBoxIs192.Size = new System.Drawing.Size(93, 17);
            this.checkBoxIs192.TabIndex = 20;
            this.checkBoxIs192.Text = "Using 192.1xx";
            this.checkBoxIs192.UseVisualStyleBackColor = true;
            this.checkBoxIs192.CheckedChanged += new System.EventHandler(this.checkBoxIs192_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 454);
            this.Controls.Add(this.checkBoxIs192);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStream);
            this.Controls.Add(this.textBoxUsername);
            this.Controls.Add(this.textBoxPasswd);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxIp);
            this.Controls.Add(this.buttonGetDevices);
            this.Controls.Add(this.listBoxCameras);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxCameras;
        private System.Windows.Forms.Button buttonGetDevices;
        private System.Windows.Forms.TextBox textBoxIp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPasswd;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Button buttonStream;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.CheckBox checkBoxIs192;

    }
}

