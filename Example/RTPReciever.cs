﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Example
{
    class RTPReciever : IDisposable
    {
        public const ushort ListenPort = 9002;
        private UdpClient _udpClient;
        private IPEndPoint _endpoint;
        private bool _done;

        public RTPReciever()
        {
            ThreadPool.QueueUserWorkItem(obj => Listen());
        }

        public void Listen()
        {
            _udpClient = new UdpClient(ListenPort);
            _udpClient.Client.ReceiveBufferSize = 1 * 1024 * 1024;
            _endpoint = new IPEndPoint(IPAddress.Any, ListenPort);
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "Video.h264");

            try
            {
                using (var stream = new FileStream(string.Format(path, 0), FileMode.Create))
                {
                    while (!_done)
                    {
                        Console.WriteLine("Waiting for broadcast");
                        byte[] bytes = _udpClient.Receive(ref _endpoint);
                        Console.WriteLine("Received broadcast from {0}", _endpoint.ToString());

                        var rtpStream = new RTPStream(bytes);

                        if (!rtpStream.IsValid) continue;

                        stream.Write(rtpStream.Buffer, 0, rtpStream.Buffer.Length);
                    }
                    stream.Flush();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                _udpClient.Close();
            }
        }

        #region IDisposable Members
        public void Dispose()
        {
            _done = true;
            _udpClient.Close();
        }
        #endregion
    }
}
