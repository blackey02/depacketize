﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Example
{
    public class RTPUtils
    {
        public static int HostToNetworkOrder(int host)
        {
            return (((IPAddress.HostToNetworkOrder((short)host) & 0xffff) << 0x10) |
                (IPAddress.HostToNetworkOrder((short)(host >> 0x10)) & 0xffff));
        }

        public static uint SwapUnsignedInt(uint source)
        {
            return (uint)((((source & 0x000000FF) << 24)
                | ((source & 0x0000FF00) << 8)
                | ((source & 0x00FF0000) >> 8)
                | ((source & 0xFF000000) >> 24)));
        }

        public static ushort HostToNetworkOrderShort(ushort host)
        {
            return (ushort)(((host & 0xFF) << 8) | ((host >> 8) & 0xFF));
        }
    }
}
