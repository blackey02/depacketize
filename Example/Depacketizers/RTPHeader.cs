﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class RTPHeader
    {
        public static int LENGTH = 12;

        private int _version;
        private byte _padding;
        private byte _extension;
        private int _csrcCount;
        private bool _marker;
        private int _payloadType;
        private ushort _sequence;
        private uint _time;
        private uint _sourceId;

        public int Version { get { return _version; } }
        public byte Padding { get { return _padding; } }
        public byte Extension { get { return _extension; } }
        public int CsrcCount { get { return _csrcCount; } }
        public bool Marker { get { return _marker; } }
        public int PayloadType { get { return _payloadType; } }
        public ushort Sequence { get { return _sequence; } }
        public uint Time { get { return _time; } }
        public uint SourceId { get { return _sourceId; } }

        public RTPHeader(byte[] buffer, int offset)
        {
            _version = buffer[offset] >> 6;
            _padding = (byte)(0x1 & (buffer[offset] >> 5));
            _extension = (byte)(0x1 & (buffer[offset] >> 4));
            _csrcCount = 0x1F & (buffer[offset]);
            _marker = ((buffer[offset+1] >> 7) == 1);
            _payloadType = buffer[offset+1] & 0x7f;
            _sequence = RTPUtils.HostToNetworkOrderShort(System.BitConverter.ToUInt16(buffer, offset+2));
            _time = RTPUtils.SwapUnsignedInt(System.BitConverter.ToUInt32(buffer, offset+4));
            _sourceId = RTPUtils.SwapUnsignedInt(System.BitConverter.ToUInt32(buffer, offset+8));
        }
    }
}
