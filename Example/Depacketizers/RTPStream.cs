﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class RTPStream
    {
        // VLC requires 4 bytes
        private static byte[] NalPrefix = new byte[] { 0x0, 0x0, 0x0, 0x1 };
        private static byte[] NalPrefix3 = new byte[] { 0x0, 0x0, 0x1 };
        private static bool FuAStartedAndNotEnded;
        private static NalBuffer NalBuffer = new NalBuffer();
        public RTPHeader _rtpHeader;
        public H264Header _h264Header;

        public RTPStream(byte[] buffer)
        {
            _rtpHeader = new RTPHeader(buffer, 0);
            _h264Header = new H264Header(buffer, RTPHeader.LENGTH);
            Decode(buffer, RTPHeader.LENGTH);
        }

        public bool IsValid
        {
            get
            {
                bool success = true;
                success = success && FuAStartedAndNotEnded == false;
                success = success && NalBuffer.Discarded == false;
                return success;
            }
        }

        public byte[] Buffer
        { 
            get
            {
                return NalBuffer.Buffer;
            }
        }

        public bool DepacketizeFullNal(byte[] buffer, int offset)
        {
            NalBuffer.Reset();
            int payloadLength = buffer.Length - offset;

            NalBuffer.AddLengthIfNecessary(NalPrefix.Length);
            Array.Copy(NalPrefix, 0, NalBuffer.Buffer, NalBuffer.Offset, NalPrefix.Length);
            NalBuffer.Offset += NalPrefix.Length;

            NalBuffer.AddLengthIfNecessary(payloadLength);
            Array.Copy(buffer, offset, NalBuffer.Buffer, NalBuffer.Offset, payloadLength);
            NalBuffer.Offset += payloadLength;

            //Console.WriteLine(string.Format("FULL Nal: {0} - NalType: {1} - Seq: {2}", 
            //    _h264Header.NalType, _h264Header.NalPayloadType, _rtpHeader.Sequence));

            return true;
        }

        public bool DepacketizeFuA(byte[] buffer, int offset)
        {
            int octet = 0;
            byte fuIndicator = buffer[offset];
            byte fuHeader = buffer[offset+1];
            offset += H264Header.LENGTH;
            int payloadLength = buffer.Length - offset;

            if(_h264Header.Start)
            {
                // Start and end must not both be set
                if(_h264Header.End)
                    return false;

                FuAStartedAndNotEnded = true;
                octet = (fuIndicator & 0xE0) | _h264Header.NalPayloadType;
            }
            else if (!FuAStartedAndNotEnded)
                return false;

            if(_h264Header.Start)
            {
                NalBuffer.Reset();
                NalBuffer.AddLengthIfNecessary(NalPrefix3.Length + 1);
                Array.Copy(NalPrefix3, 0, NalBuffer.Buffer, NalBuffer.Offset, NalPrefix3.Length);
                NalBuffer.Offset = NalPrefix3.Length;
                NalBuffer.Buffer[NalBuffer.Offset] = (byte)(octet & 0xFF);
                NalBuffer.Offset++;
            }

            NalBuffer.AddLengthIfNecessary(payloadLength);
            Array.Copy(buffer, offset, NalBuffer.Buffer, NalBuffer.Offset, payloadLength);
            NalBuffer.Offset += payloadLength;

            if (_h264Header.End)
                FuAStartedAndNotEnded = false;

            //Console.WriteLine(string.Format("Nal: {0} - NalType: {1} - Seq: {2}",
            //    _h264Header.NalType, _h264Header.NalPayloadType, _rtpHeader.Sequence));

            return true;
        }

        public void Decode(byte[] buffer, int offset)
        {
            if (_h264Header.NalType == 7 || _h264Header.NalType == 8)
            {
                DepacketizeFullNal(buffer, offset);
                FuAStartedAndNotEnded = false;
            }
            else if (_h264Header.NalType == 28)
            {
                bool depacketized = DepacketizeFuA(buffer, offset);
                if (!depacketized)
                {
                    NalBuffer.Discarded = true;
                    FuAStartedAndNotEnded = false;
                }
            }
            else
            {
                //Console.WriteLine(string.Format("Unsupported NalType: {0}", _h264Header.NalType));
                NalBuffer.Discarded = true;
                FuAStartedAndNotEnded = false;
            }
        }
    }
}
