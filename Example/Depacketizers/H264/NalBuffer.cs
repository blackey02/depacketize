﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class NalBuffer
    {
        private byte[] _buffer = new byte[0];

        public byte[] Buffer { private set { _buffer = value; } get { return _buffer; } }
        public int BufferLength { get { return _buffer.Length; } }
        public int Offset { get; set; }
        public bool Discarded { get; set; }

        public void SetLength(int length)
        {
            Buffer = new byte[length];
        }

        public void AddLength(int length)
        {
            int totalLength = BufferLength + length;
            var newBuffer = new byte[totalLength];
            Array.Copy(Buffer, 0, newBuffer, 0, BufferLength);
            Buffer = newBuffer;
        }

        public void AddLengthIfNecessary(int length)
        {
            int available = BufferLength - Offset;
            if (available < length)
            {
                int delta = length - available;
                AddLength(delta);
            }
        }

        public void Reset()
        {
            Buffer = new byte[0];
            Offset = 0;
            Discarded = false;
        }
    }
}
