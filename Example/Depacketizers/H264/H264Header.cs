﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class H264Header
    {
        public static int LENGTH = 2;

        private int _nalType;
        private int _nalPayloadType;
        private bool _start;
        private bool _end;

        public int NalType { get { return _nalType; } }
        public int NalPayloadType { get { return _nalPayloadType; } }
        public bool Start { get { return _start; } }
        public bool End { get { return _end; } }

        public H264Header(byte[] buffer, int offset)
        {
            _nalType = buffer[offset] & 0x1F;
            _nalPayloadType = buffer[offset+1] & 0x1F;
            _start = ((buffer[offset+1] & 0x80) >> 7) == 1;
            _end = ((buffer[offset+1] & 0x40) >> 6) == 1;
        }
    }
}
