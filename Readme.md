Depacketize
====================

Depacketize?
---------------------
Depacketize is a project that currently takes in h264 packetized RTP packets from a camera and depacketizes them and writes the output to a *.h264 file to be played back via VLC. 

Summary
---------------------
This application is specifically tailored to run an a Pelco Endura network where it first connects to a SM, then starts the streaming/depacketizing from the selected camera. I hope to later add mpeg4 and decoding. 

Building
---------------------
0. Double click the Visual Studio 2010 solution file

Screens
---------------------
Application has started

![1](https://copy.com/V9oiwGEn7IYd)

We've acquired all cameras from the Endura SM

![2](https://copy.com/BIpAk1ydi60n)

Select a camera and click stream 

![3](https://copy.com/2PvVMUuzSx6I)

A video file is written out to your desktop

![4](https://copy.com/I40Nt2gaPH70)

[Example Video](https://copy.com/WcEPKbig7KWz)

Acknowledgements
---------------------
* [Pelco](http://pdn.pelco.com)