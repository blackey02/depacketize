﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace DeviceRegister
{
    [XmlRoot(Namespace = "", ElementName = "evHeader")]
    public class EVHeader : SoapHeader
    {
        [XmlElement(ElementName = "userID")]
        public int UserId { get; set; }
        public string UCN { get { return String.Format("uuid:{0}", Guid.NewGuid()); } }
    }
}
