﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeviceRegister
{
    internal class RegisterHelper
    {
        private string _ipAddress;
        private ushort _port;
        private ushort _serviceId;
        private DeviceRegistryNs.DeviceRegistry _deviceRegistryProxy;

        public RegisterHelper(string ipAddress, ushort port, ushort serviceId)
        {
            _ipAddress = ipAddress;
            _port = port;
            _serviceId = serviceId;
            _deviceRegistryProxy = new DeviceRegistryNs.DeviceRegistry();
            _deviceRegistryProxy.Url = ControlUrl;
        }

        private string ControlUrl
        {
            get
            {
                return String.Format(@"http://{0}:{1}/control/DeviceRegistry-{2}", _ipAddress, _port, _serviceId);
            }
        }

        public bool GetDevices(Dictionary<string, DeviceInfo> devices, DeviceType[] deviceTypes, QueryFlag[] queryFlags,
            UpnpState[] states, int loginId, ref int seqId)
        {
            DeviceType[] types = deviceTypes ?? new DeviceType[] { 
                DeviceType.Camera, 
                DeviceType.Encoder, 
                DeviceType.Alarm, 
                DeviceType.AlarmArray,
                DeviceType.Relay,
                DeviceType.RelayArray,
                DeviceType.Transcoder,
                DeviceType.Decoder,
                DeviceType.Monitor,
                DeviceType.NetworkVideoRecorder,
                DeviceType.VCD,
                DeviceType.SystemLogDevice,
                DeviceType.RemoteUI,
                DeviceType.UniversalDeviceInput,
                DeviceType.VirtualAlarmArray
            };

            foreach (DeviceType deviceType in types)
            {
                bool success = true;
                int maxTries = 5;
                do
                {
                    success = GetDevices(devices, deviceType.ToString(), queryFlags, states, loginId, ref seqId);
                    maxTries--;
                } while (!success && maxTries > 0);
            }
            return true;
        }

        public bool GetDevices(Dictionary<string, DeviceInfo> devices, DeviceType[] deviceTypes, int loginId, ref int seqId)
        {
            return GetDevices(devices, deviceTypes, null, null, loginId, ref seqId);
        }

        public bool GetDevices(Dictionary<string, DeviceInfo> devices, DeviceType deviceType, int loginId, ref int seqId)
        {
            return GetDevices(devices, deviceType.ToString(), null, null, loginId, ref seqId);
        }

        private bool GetDevices(Dictionary<string, DeviceInfo> devices, string deviceType, QueryFlag[] queryFlags, UpnpState[] states, int loginId, ref int seqId)
        {
            EVHeader evHeader = new EVHeader();
            evHeader.UserId = loginId;
            _deviceRegistryProxy.CustomHeader = evHeader;

            DeviceRegistryNs.UpnpDvcQuery query = new DeviceRegistryNs.UpnpDvcQuery();

            query.queryFlagsSpecified = true;
            query.queryFlags = GetQueryFlagValue(queryFlags ?? new QueryFlag[] { 
                QueryFlag.GetTypes, QueryFlag.GetStates, QueryFlag.AllAttributes, QueryFlag.AllServiceInfo
            });

            query.upnpStates = GetUpnpStates(states ?? new UpnpState[] { UpnpState.Online, UpnpState.Offline });
            query.dvcTypeQueryStrs = GetDeviceTypesQueryStr(deviceType);
            query.stateDeltaSeqNum = String.Format("{0}", seqId);

            DeviceRegistryNs.Page page = new DeviceRegistryNs.Page();

            try
            {
                _deviceRegistryProxy.GetDevices(query, string.Empty, out page);
            }
            catch (System.Net.WebException)
            {
                return false;
            }
            catch (System.Web.Services.Protocols.SoapException)
            {
                return false;
            }

            if (page.item == null)
                return true;

            foreach (DeviceRegistryNs.PageItem item in page.item)
            {
                DeviceInfo deviceInfo = new DeviceInfo();
                deviceInfo.UDN = item.udn;
                deviceInfo.State = (UpnpState)item.state;

                deviceInfo.Attributes = new Dictionary<string, string>();
                if (item.attrs != null)
                {
                    foreach (DeviceRegistryNs.AttrItem attr in item.attrs)
                        deviceInfo.Attributes[attr.name] = attr.value;
                }

                deviceInfo.Services = new List<ServiceInfo>();
                if (item.svcInfo != null)
                {
                    foreach (DeviceRegistryNs.ServiceInfoItem srvc in item.svcInfo)
                    {
                        ServiceInfo srvcInfo = new ServiceInfo();
                        srvcInfo.Id = srvc.id;
                        srvcInfo.State = srvc.state;
                        srvcInfo.Type = srvc.type;
                        deviceInfo.Services.Add(srvcInfo);
                    }
                }
                devices[item.udn] = deviceInfo;
            }
            return true;
        }

        private static DeviceRegistryNs.DvcTypeQueryStrs GetDeviceTypesQueryStr(string deviceType)
        {
            DeviceRegistryNs.DvcTypeQueryStrs dvcTypeQryStr = new DeviceRegistryNs.DvcTypeQueryStrs();
            DeviceRegistryNs.DvcTypeItem dvcTypeItem = new DeviceRegistryNs.DvcTypeItem();
            dvcTypeItem.str = String.Format("urn:schemas-pelco-com:device:{0}:1", deviceType);
            dvcTypeQryStr.item = dvcTypeItem;
            return dvcTypeQryStr;
        }

        private static int GetQueryFlagValue(params QueryFlag[] flags)
        {
            int value = 0;
            foreach (QueryFlag flag in flags)
                value |= (int)flag;
            return value;
        }

        private static DeviceRegistryNs.UpnpStates[] GetUpnpStates(params UpnpState[] states)
        {
            DeviceRegistryNs.UpnpStates[] dvcRegStates = new DeviceRegistryNs.UpnpStates[states.Length];
            for (int i = 0; i < states.Length; i++)
                dvcRegStates[i] = GetUpnpState(states[i]);
            return dvcRegStates;
        }

        private static DeviceRegistryNs.UpnpStates GetUpnpState(UpnpState state)
        {
            DeviceRegistryNs.UpnpStates upnpState = new DeviceRegistryNs.UpnpStates();
            upnpState.upnpState = (int)state;
            upnpState.upnpStateSpecified = true;
            return upnpState;
        }
    }
}
