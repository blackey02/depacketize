﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using System.Xml;

namespace UserAndRoleNs
{
    public partial class UserAndRole
    {
        public SoapHeader CustomHeader { get; set; }

        protected override XmlWriter GetWriterForMessage(SoapClientMessage message, int bufferSize)
        {
            if (this.CustomHeader != default(SoapHeader))
                message.Headers.Add(this.CustomHeader);
            return base.GetWriterForMessage(message, bufferSize);
        }
    }
}
