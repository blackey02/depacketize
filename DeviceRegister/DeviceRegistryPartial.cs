﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web.Services.Protocols;

namespace DeviceRegistryNs
{
    public partial class DeviceRegistry
    {
        public SoapHeader CustomHeader { get; set; }

        protected override XmlWriter GetWriterForMessage(SoapClientMessage message, int bufferSize)
        {
            if (this.CustomHeader != default(SoapHeader))
                message.Headers.Add(this.CustomHeader);
            return base.GetWriterForMessage(message, bufferSize);
        }
    }
}
