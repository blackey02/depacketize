﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeviceRegister
{
    public enum UpnpState
    {
        None = 0,
        InitSync = 1,
        Online = 2,
        Offline = 3,
        OutOfSync = 4,
        Invalid = 5
    }

    internal enum QueryFlag
    {
        GetDatabaseIds = 1,
        GetTypes = 2,
        GetStates = 4,
        RootOnly = 8,
        AllAttributes = 16,
        AllServiceInfo = 32,
        GetExpireTimes = 64
    }

    public enum DeviceType
    {
        Alarm = 1,
        AlarmArray = 2,
        Basic = 3,
        Camera = 4,
        Decoder = 5,
        Encoder = 6,
        EventArbiter = 7,
        Gateway = 8,
        MappingUI = 9,
        MDD = 10,
        Monitor = 11,
        MDDMonitor = 12,
        NSD = 13,
        NetworkVideoRecorder = 14,
        NSM = 15,
        Pelco = 16,
        Relay = 17,
        RelayArray = 18,
        RemoteUI = 19,
        StorageExpansionBox = 20,
        SystemManagerLocat0rDevice = 21,
        ScriptManager = 22,
        SystemManagerDevice = 23,
        SystemLogDevice = 24,
        Transcoder = 25,
        UniversalDeviceInput = 26,
        VirtualAlarmArray = 27,
        VCD = 28
    }

    public struct ServiceInfo
    {
        public int? State;
        public string Id;
        public string Type;
    }

    public struct DeviceInfo
    {
        public string UDN;
        public Dictionary<string, string> Attributes;
        public List<ServiceInfo> Services;
        public UpnpState State;

        public override string ToString()
        {
            return Attributes["SYS_UpnpFriendlyName"];
        }
    }
}
